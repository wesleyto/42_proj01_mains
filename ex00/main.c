#include <stdio.h>
#include "match.c"

int main(void)
{
	char match1[] = "*.c";
	char string1[] = "main.c";

	char match2[] = "****.c";
	char string2[] = "main.c";
	
	char match3[] = "*.c*";
	char string3[] = "main.c";
	
	char match4[] = "*.b";
	char string4[] = "main.c";
	
	char match5[] = ".c";
	char string5[] = "main.c";
	
	char match6[] = "*";
	char string6[] = "main.c";
	
	char match7[] = "*********";
	char string7[] = "main.c";

	char match8[] = "m*";
	char string8[] = "main.c";

	char match9[] = "*m*";
	char string9[] = "main.c";
	
	char match10[] = "m*n*";
	char string10[] = "main.c";
	
	char match11[] = "*m*n*";
	char string11[] = "main.c";
	
	char match12[] = "*m**n**";
	char string12[] = "main.c";
	
	char match13[] = "main*c";
	char string13[] = "main.c";
	
	char match14[] = "***main.c***";
	char string14[] = "main.c";
	
	char match15[] = "main.c***";
	char string15[] = "main.c";
	
	char match16[] = "m***";
	char string16[] = "m";
	
	char match17[] = "*";
	char string17[] = "";
	
	char match18[] = "***";
	char string18[] = "m";
	
	char match19[] = "***";
	char string19[] = "";
	
	char match20[] = "ma";
	char string20[] = "mb";
	
	char match21[] = "m";
	char string21[] = "";

	char match22[] = "";
	char string22[] = "m";

	char match23[] = "*";
	char string23[] = "*a";
	
	char match24[] = "**";
	char string24[] = "*";

	char match25[] = "*b";
	char string25[] = "*ab";

	char match26[] = "a*";
	char string26[] = "a*a";

	char match27[] = "*a";
	char string27[] = "**a";

	int exps[] = {1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1};
	
	char *matches[] = {match1, match2, match3, match4, match5,
					match6, match7,	match8, match9, match10,
					match11, match12, match13, match14, match15,
					match16, match17, match18, match19, match20,
					match21, match22, match23, match24, match25,
					match26, match27};
	char *strings[] = {string1, string2, string3, string4, string5,
					string6, string7, string8, string9, string10,
					string11, string12, string13, string14, string15,
					string16, string17, string18, string19, string20,
					string21, string22, string23, string24, string25,
					string26, string27};
	
	char output[] = "%s || Exp: %-8s || Got: %-8s || Case: \"%s\" \"%s\"\n";
	
	for (int i = 0; i < (int)(sizeof(exps) / sizeof(int)); i++)
	{
		int result = match(strings[i], matches[i]);
		char *res = result == exps[i] ? "Success" : "Failure";
		printf(output, res, exps[i] == 1 ? "Match" : "No Match", result == 1 ? "Match" : "No Match", strings[i], matches[i]);
	}
}